# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.4-preview] - 2020-03-22
### Added
- Now validates `typeId`, checking if a referenced Type name has changed.
- Shows an error in the inspector if the `typeId` doesn't resolve into a type.
- Shows a warning in the inspector if the `SerializableType` doesn't have a `GuidAttribute` defined.
- Emits a `Debug.LogError` during the build step if a type can't be resolved from the typeId.

### Removed
- Replaced `TypeIdAttribute` with Microsoft's `GuidAttribute`, since this is the intended use-case.

### Fixed
- Changing the value of the `SerializableType` property is not marking the scene dirty.
- Changing the name of a type referenced by a `SerializableType` is setting the `typeId` to `string.Empty`.

## [0.0.3-preview] - 2020-03-16
### Added
- Many utility scripts have been removed from this package and added to the Unity Editor Extensions package.
- Changed `TypeSerializableAttribute` to `TypeIdAttribute`.

## [0.0.2-preview] - 2020-03-08
### Added
- `TypeSerializableAttribute` that allows users to specify an `Id` field. This allows the serialized type to change names without breaking.

