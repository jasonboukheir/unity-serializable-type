﻿using System;
using UnityEngine;
using System.Runtime.InteropServices;

#if UNITY_EDITOR
using UnityEditor.Build.Reporting;
using UnityEditor.Build;
using UnityEditor;
using System.Linq;
#endif // UNITY_EDITOR

namespace UnitySerializableType
{
    [Serializable]
    public partial class SerializableType : ISerializationCallbackReceiver
    {
        [SerializeField]
        protected string typeId = string.Empty;

        public Type Type { get; set; }

#if UNITY_EDITOR
        public static bool isBuilding = false;
#endif // UNITY_EDITOR

        public static Type ToType(string typeId)
        {
#if UNITY_EDITOR
            if (Guid.TryParse(typeId, out var guid))
                return TypeCache.GetTypesWithAttribute(typeof(GuidAttribute))
                    .FirstOrDefault(t => t.GUID == guid);
            else
                return Type.GetType(typeId);
#else
            return Type.GetType(typeId);
#endif // UNITY_EDITOR
        }

        public static string ToSerializedType(Type type)
        {
            if (type == null) return string.Empty;
#if UNITY_EDITOR
            if (Attribute.IsDefined(type, typeof(GuidAttribute)) && !isBuilding)
                return type.GUID.ToString();
            else
                return type.AssemblyQualifiedName;
#else
            return type.AssemblyQualifiedName;
#endif // UNITY_EDITOR
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            Type = ToType(typeId);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            var value = ToSerializedType(Type);
            if (typeId != value && !string.IsNullOrEmpty(value))
            {
                typeId = value;
            }
        }
    }

#if UNITY_EDITOR
    public partial class SerializableType : IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
        public int callbackOrder => 0;

        void IPostprocessBuildWithReport.OnPostprocessBuild(BuildReport report)
        {
            isBuilding = false;
        }

        void IPreprocessBuildWithReport.OnPreprocessBuild(BuildReport report)
        {
            isBuilding = true;
        }
    }
#endif // UNITY_EDITOR
}
