# To install:

Add the following line to your manifest.json dependencies:

```
"dependencies": {
    "com.jasonboukheir.unity-serializable-type": "https://gitlab.com/jasonboukheir/unity-serializable-type.git?path=/UnitySerializableType.Unity/Packages/com.jasonboukheir.unity-serializable-type",
    ...
}
```