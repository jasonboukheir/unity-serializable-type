﻿using System.Runtime.InteropServices;

public interface IBase { }

public abstract class Base : IBase { }

public class Derived1Renamed : Base { }

[Guid("3011c58c-8f35-47f6-908d-68f9b5f0c538")]
public class Derived2 : Base { }
