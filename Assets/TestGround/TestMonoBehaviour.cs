﻿using UnityEngine;
using UnitySerializableType;

public class TestMonoBehaviour : MonoBehaviour
{
    [DerivedFrom(typeof(IBase))]
    public SerializableType type;
}
